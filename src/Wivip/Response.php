<?php
namespace Wivip;

class Response {

    public function __construct($raw)
    {
        $this->raw = $raw;
    }

    public static function __callStatic($method, $args)
    {
        $method = strtoupper($method) . ' ';
        $args = array_map(function ($arg) {
            return trim($arg);
        }, $args);

        $end = ':' . array_pop($args);
        $args[] = $end;

        return new static($method . implode(' ', $args));
    }

    public function __toString()
    {
        return $this->raw;
    }
}