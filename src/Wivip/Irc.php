<?php
namespace Wivip;

use Monolog\Logger;
use Wivip\Plugin\PluginAbstract;
use Symfony\Component\EventDispatcher\EventDispatcher;

class Irc extends \Pimple{

    public function __construct(array $config = array())
    {
        $irc = $this;
        $this['config'] = $config;
        $this['logger'] = $this->share(function () use ($irc) {
            $logger = new Logger($irc->getConfig('logger.name'));
            $handlerClass = $irc->getConfig('logger.handler');
            $handler = new $handlerClass($irc->getConfig('logger.path'));
            $formatterClass = $irc->getConfig('logger.formatter');
            $formatter = new $formatterClass($irc->getConfig('logger.format', true), $irc->getConfig('logger.dateFormat', true));
            $handler->setFormatter($formatter);
            $logger->pushHandler($handler);

            return $logger;
        });
        $this['dispatcher'] = $this->share(function () {
            return new EventDispatcher;
        });

        //cache
        $this['debug'] = $this->getConfig('debug');

        $this->on('server.ping', function ($e) {
            $e->addResponse(Response::pong($e->getRequest()->getMessage()));
        }, 1);
    }

    /**
     * Get a config value or return a value from defaults
     * @param  string $name config value to get separated by a .
     * @return mixed
     */
    public function getConfig($name, $nullable = false)
    {
        $defaults = array(
            'debug' => false,
            'interval' => 60,
            'logger' => array(
                'name' => 'My IRC Bot',
                'path' => 'php://stderr',
                'handler' => 'Monolog\\Handler\\StreamHandler',
                'formatter' => 'Monolog\\Formatter\LineFormatter',
                'format' => null,
                'dateFormat' => null,
                'login' => false
            )
        );

        $configValue = arrayGet($this['config'], $name);

        if (is_null($configValue)) {
            $configValue = arrayGet($defaults, $name);
        }
        if (is_null($configValue) and ! $nullable) {
            throw new MissingConfigException('Missing required config key: ' . $name);
        }

        return $configValue;
    }

    public function loadPlugin($plugin)
    {
        if ( ! $plugin instanceof PluginAbstract and is_string($plugin)) {
            if ( ! class_exists($plugin)) {
                throw new PluginNotFoundException('Plugin does not exist.');
            }
            $plugin = new $plugin($this);
        }

        if ( ! $plugin instanceof PluginAbstract) {
            throw new InvalidPluginException('Plugin must extend PluginAbstract.');
        }

        $plugin->setBot($this);
        $this[$plugin->getName()] = $plugin;
        $plugin->init();
    }

    public function on($event, \Closure $callback, $priority = 0)
    {
        $this['dispatcher']->addListener($event, $callback, $priority);
    }

    public function run()
    {
        if ($this->connect()) {
            $this->login();
            $this->join();
            $this->listen();
        }
    }

    public function dispatch($eventName, Event $event) {
        $event = $this['dispatcher']->dispatch($eventName, $event);
        $this->sendResponses($event->getResponses());
    }

    protected function sendResponses(array $responses)
    {
        if (!empty($responses)) {
            foreach ($responses as $response) {
                $this->send($response);

            }
        }
    }

    protected function connect()
    {
        stream_set_blocking(STDIN, 0);
        $this['socket'] = fsockopen($this->getConfig('host'),
                                    $this->getConfig('port'));

        return (bool) $this['socket'];
    }

    protected function login()
    {
        $this->send(Response::nick($this->getConfig('user.nick')));
        $this->send(Response::user(
            $this->getConfig('user.nick'),
            $this->getConfig('host'),
            $this->getConfig('user.serverName'),
            $this->getConfig('user.realName')
        ));

        $this->dispatch('server.login', new Event);
    }

    protected function join()
    {
        foreach ($this->getConfig('channels') as $channel) {
            $this->send(Response::join($channel));
        }
    }

    protected function listen()
    {
        do {
            $data = fgets($this['socket']);
            if ($data !== false) {
                $request   = new Request($data);
                $eventName = $this->getEventName($request);

                $this->debug('in', $data);

                // Skip processing if the incoming message is from the bot
                if ($request->getSendingUser() === $this->getConfig('user.nick')) {
                    continue;
                }

                $event = new Event($request);
                $this->dispatch($eventName, $event);
            }
        } while ( ! feof($this['socket']));
    }

    protected function getEventName(Request $request)
    {
        $cmd = strtolower($request->getCommand());
        $eventName = 'server.' . $cmd;

        if ($cmd === 'privmsg') {
            $eventName = 'message.' . ($request->isPrivateMessage() ? 'private' : 'channel');
        }

        return $eventName;
    }

    protected function send($response)
    {
        $this->debug('out', $response);
        $response .= "\r\n";
        fwrite($this['socket'], $response, 512);

        if (strpos($response, 'PRIVMSG') === 0 and strpos($response, 'NickServ') === false) {
            sleep($this->getConfig('interval'));
        }
    }

    protected function debug($type, $data)
    {
        if ($this['debug']) {
            if ( ! $this->getConfig('logger.login') and $type == 'in') {
                return;
            }
            $this['logger']->debug($data);
        }
    }
}

class MissingConfigException extends \Exception {}
class PluginNotFoundException extends \Exception {}
class InvalidPluginException extends \Exception {}