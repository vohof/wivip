<?php
namespace Wivip;

class Event extends \Symfony\Component\EventDispatcher\Event {

    private $request;
    private $responses = array();

    public function __construct(Request $request = null)
    {
        $this->request = $request;
    }

    public function addResponse(Response $response)
    {
        $this->responses[] = $response;
    }

    public function getResponses()
    {
        return $this->responses;
    }

    public function getRequest()
    {
        return $this->request;
    }
}