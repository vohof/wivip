<?php
namespace Wivip\Plugin;

use Wivip\Irc;

abstract class PluginAbstract {

    /**
     * @var Wivip\Irc
     */
    public $bot;

    /**
     * Set the bot instance
     * @param Irc $bot
     */
    public function setBot(Irc $bot)
    {
        $this->bot = $bot;
    }

    /**
     * Get the name of the plugin
     * @return string
     */
    public abstract function getName();

    /**
     * Initialize the plugin
     * @return void
     */
    public abstract function init();
}