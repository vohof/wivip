<?php
use Wivip\Irc;
use Wivip\Response;
use Wivip\Request;
use Wivip\Event;


class IrcTest extends PHPUnit_Framework_TestCase {

    public function setUp()
    {
        $this->config = array(
            'host' => 'irc.freenode.net',
            'port' => '6667',
            'user' => array(
                'nick' => 'Hakuha343',
                'realName' => 'Lion King',
                'serverName' => 'eer.yolo.com',
            ),
            'logger' => array(
                'name' => 'foo'
            )
        );
        $this->irc = new Irc($this->config);
    }

    public function testgetConfigCanGetValues()
    {
        $this->assertEquals('foo', $this->irc->getConfig('logger.name'));

        // default value if not set
        $this->assertEquals('My IRC Bot', (new Irc)->getConfig('logger.name'));
    }

    /**
     * @expectedException Wivip\MissingConfigException
     */
    public function testConfigThrowsExceptionWhenInvalidConfigKey()
    {
        $this->assertEquals('foo', $this->irc->getConfig('Key.Which.Does.Not.Exist'));
    }

    /**
     * description
     */
    public function testConfigCanGetNullableValue()
    {
        $this->assertNull($this->irc->getConfig('logger.dateFormat', true));
    }

    public function testLoggerIsInstanceofMonolog()
    {
        $this->assertInstanceOf('Monolog\\Logger', $this->irc['logger']);
    }

    /**
     * @expectedException Wivip\PluginNotFoundException
     */
    public function testLoadPluginFailsIfNotExist()
    {
        $this->irc->loadPlugin('TheNoneExistentClass');
    }

    /**
     * @expectedException Wivip\InvalidPluginException
     */
    public function testLoadPluginFailsIfNotValidInstance()
    {
        $this->irc->loadPlugin(new stdClass);
    }

    public function testPluginIsInitialized()
    {
        $plugin = new FooPlugin;
        $this->irc->loadPlugin($plugin);
        $this->assertAttributeEquals(true, 'initialized', $plugin);
    }

    public function testEventListenerGetsExecuted()
    {
        $response = Wivip\Response::privmsg('NickServ', 'foo');
        $mock = $this->getMock('Wivip\\Irc', array('sendResponses'));
        $mock->expects($this->once())->method('sendResponses')->will($this->returnCallback(function ($responses) use ($response){
            $this->assertEquals($response, $responses[0]);
        }));
        $mock->on('server.join', function ($event) use ($response){
            $event->addResponse($response);
        });
        $mock->dispatch('server.join', new Wivip\Event);
    }

}

class FooPlugin extends Wivip\Plugin\PluginAbstract {
    private $initialized = false;
    public function getName() {}
    public function init() {
        $this->initialized = true;
    }
}